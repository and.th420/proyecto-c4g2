package com.andresggiraldo.misiontic.appturismo.presenter;

import com.andresggiraldo.misiontic.appturismo.model.RegistroInteractor;
import com.andresggiraldo.misiontic.appturismo.model.MasVisitadosInteractor;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Visitados;
import com.andresggiraldo.misiontic.appturismo.mvp.RegistroMVP;
import com.andresggiraldo.misiontic.appturismo.mvp.MasVisitadosMVP;

import java.util.List;

public class MasVisitadosPresenter implements MasVisitadosMVP.Presenter {

    private MasVisitadosMVP.View view;
    private MasVisitadosMVP.Model model;

    public MasVisitadosPresenter(MasVisitadosMVP.View view) {
        this.view = view;
        this.model = (MasVisitadosMVP.Model) new MasVisitadosInteractor(view.getActivity());
    }


    @Override
    public void obtemasVisitados() {

        //MasVisitadosMVP.MasVisitadosInfo masVisitadosInfo = view.getMasVisitadosInfo();


        model.obtenerMasVisitados(
                new MasVisitadosMVP().Model.MasVisitadosCallback() {
                    @Override
                    public void onSuccess(List<Visitados> listaMasVisitados) {
                        view.setlistaMasVisitados(listaMasVisitados);

                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
    }
}
