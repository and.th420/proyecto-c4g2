package com.andresggiraldo.misiontic.appturismo.model.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.andresggiraldo.misiontic.appturismo.model.database.dao.UserDao;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;

@Database(entities = {User.class},version = 2)
public abstract class TurismoDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

    //atributo
    private static volatile TurismoDatabase Instance;

    public static TurismoDatabase getDatabase(Context context) {
        if (Instance ==null){
            Instance = Room.databaseBuilder(context.getApplicationContext(),
                    TurismoDatabase.class,"database-name")
                    .allowMainThreadQueries()
                    .build();
        }


        return Instance;
    }
}


