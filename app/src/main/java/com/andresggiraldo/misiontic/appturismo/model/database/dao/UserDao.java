package com.andresggiraldo.misiontic.appturismo.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("Select* from user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE email=:email")
    User getUserByEmail(String email);

    @Insert
    Void insert(User...users);

    @Delete
    Void deleted (User user);

}
