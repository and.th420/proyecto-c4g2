package com.andresggiraldo.misiontic.appturismo.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.adapter.AdapterMasVisitados;
import com.andresggiraldo.misiontic.appturismo.model.MasVisitados;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Visitados;
import com.andresggiraldo.misiontic.appturismo.mvp.RegistroMVP;
import com.andresggiraldo.misiontic.appturismo.mvp.MasVisitadosMVP;
import com.andresggiraldo.misiontic.appturismo.presenter.RegistroPresenter;
import com.andresggiraldo.misiontic.appturismo.presenter.MasVisitadosPresenter;

import java.util.ArrayList;
import java.util.List;

public class MasVisitadosActivity extends AppCompatActivity implements MasVisitadosMVP.View{

    AdapterMasVisitados adapter;

    private MasVisitadosMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mas_visitados);

        RecyclerView recycleMasVisitados=findViewById(R.id.rest_recycle);
        recycleMasVisitados.setLayoutManager(new GridLayoutManager(this,2));

        adapter=new AdapterMasVisitados();
        recycleMasVisitados.setAdapter(adapter);

        presenter=new MasVisitadosPresenter(this);


    }

    @Override
    protected void onResume() {
        super.onResume();

        //llenarMasVisitados();

        presenter.obtemasVisitados();
    }

        private void llenarMasVisitados() {
            ArrayList<MasVisitados> listaMasVisitados = new ArrayList<>();
            listaMasVisitados.add(new MasVisitados("MasVisitados",R.drawable.masvisit1));
            listaMasVisitados.add(new MasVisitados("MasVisitados1",R.drawable.masvisit2));
            listaMasVisitados.add(new MasVisitados("MasVisitados2",R.drawable.masvisit3));
            listaMasVisitados.add(new MasVisitados("MasVisitados3",R.drawable.masvisit4));
            listaMasVisitados.add(new MasVisitados("MasVisitados4",R.drawable.masvisit5));
            listaMasVisitados.add(new MasVisitados("MasVisitados5",R.drawable.masvisit1));
            listaMasVisitados.add(new MasVisitados("MasVisitados6",R.drawable.masvisit2));
            listaMasVisitados.add(new MasVisitados("MasVisitados7",R.drawable.masvisit3));
            listaMasVisitados.add(new MasVisitados("MasVisitados8",R.drawable.masvisit4));
            listaMasVisitados.add(new MasVisitados("MasVisitados9",R.drawable.masvisit5));


        //adapter.setData(listaMasVisitados);
    }
    @Override
    public Activity getActivity() {
        return this;
    }


    @Override
    public void setlistaMasVisitados(List<Visitados> listaMasVisitados) {
        adapter.setData(listaMasVisitados);
    }
}