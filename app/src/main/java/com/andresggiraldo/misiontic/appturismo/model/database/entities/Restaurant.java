package com.andresggiraldo.misiontic.appturismo.model.database.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class Restaurant {

    @PrimaryKey(autoGenerate = true)
    private int ID;

    private String nombre;
    private String foto;

    public Restaurant(int ID, String nombre, String foto) {
        this.ID = ID;
        this.nombre = nombre;
        this.foto = foto;
    }


    public Restaurant() {

    }

    public int getID() {return ID;  }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

}
