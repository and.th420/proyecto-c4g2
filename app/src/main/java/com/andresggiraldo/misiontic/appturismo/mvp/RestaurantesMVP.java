package com.andresggiraldo.misiontic.appturismo.mvp;

import android.app.Activity;

import com.andresggiraldo.misiontic.appturismo.model.Restaurantes;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;

import java.util.ArrayList;
import java.util.List;

public interface RestaurantesMVP {

    interface Model {
        void obtenerRestaurantes(RestauranteCallback callback);

        interface RestauranteCallback {
            void onSuccess(List<Restaurant> listaRestaurantes);

            void onFailure(String error);
        }

    }

    interface Presenter {
        void obterestaurante();

    }

    interface View {
        Activity getActivity();

       // RestauranteInfo getRestaurantesInfo();

        void setListaRestaurante(List<Restaurant> listRestaurantes);


    }

    class RestauranteInfo {
        private final String nombres;
        private String foto;


        public RestauranteInfo(String nombres, String foto) {
            this.nombres = nombres;
            this.foto = foto;

        }

        public String getNombres() {
            return nombres;
        }

        public String getApellidos() {
            return foto;
        }

    }
}


