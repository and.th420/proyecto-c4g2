package com.andresggiraldo.misiontic.appturismo.model.repository;

import android.content.Context;
import android.util.Log;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class RestaurantRepository {

    private static RestaurantRepository instance;

    public static RestaurantRepository getInstance(Context context) {
        if (instance == null) {
            instance = new RestaurantRepository(context);
        }
        return instance;
    }

    private DatabaseReference restaurantesRef;

    private RestaurantRepository(Context context) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        restaurantesRef = database.getReference("restaurantes");


    }



    public void getAll(RestCallback<List<Restaurant>> callback) {
        restaurantesRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DataSnapshot dataSnapshot = task.getResult();
                if (dataSnapshot.hasChildren()) {
                    List<Restaurant> restaurantes = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Restaurant restaurante = snapshot.getValue(Restaurant.class);
                        restaurantes.add(restaurante);
                    }
                    callback.onSuccess(restaurantes);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }
            } else {
                callback.onFailure();
            }
        });
    }

    public interface RestCallback<T> {
        void onSuccess(T data);
        void onFailure();
    }
}
