package com.andresggiraldo.misiontic.appturismo.model;

public class MasVisitados {
    private String nombre;
    private int foto;

    public MasVisitados() {
    }


    public MasVisitados(String nombre, int foto) {
        this.nombre = nombre;
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
}
