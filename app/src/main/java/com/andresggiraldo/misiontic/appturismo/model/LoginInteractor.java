package com.andresggiraldo.misiontic.appturismo.model;

import android.content.Context;
import android.content.Intent;

import com.andresggiraldo.misiontic.appturismo.model.repository.FirebaseAuthRepository;
import com.andresggiraldo.misiontic.appturismo.model.repository.GmailAuthRepository;
import com.andresggiraldo.misiontic.appturismo.model.repository.UserRepository;
import com.andresggiraldo.misiontic.appturismo.mvp.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private UserRepository userRepository;

    private FirebaseAuthRepository firebaseAuthRepository;
    private GmailAuthRepository gmailAuthRepository;

    public LoginInteractor(Context context) {
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
        gmailAuthRepository = GmailAuthRepository.getInstance(context);
        userRepository = UserRepository.getInstance(context);

    }

    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.authenticate(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure("Credenciales inválidas");
                    }
                });

    }

    @Override
    public boolean isAuthenticated() {
        return firebaseAuthRepository.isAuthenticated();
    }

    @Override
    public Intent getGoogleSignInIntent() {
        return gmailAuthRepository.getSignInIntent();
    }

    @Override
    public void setGoogleData(Intent data, ValidateCredentialsCallback callback) {
        gmailAuthRepository.setLoginData(data, new GmailAuthRepository.GoogleAuthCallback() {
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
                callback.onFailure("Autenticación no finalizada");
            }
        });
    }
}