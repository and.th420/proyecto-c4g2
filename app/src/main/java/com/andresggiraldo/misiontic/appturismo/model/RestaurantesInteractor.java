package com.andresggiraldo.misiontic.appturismo.model;

import android.content.Context;

import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.andresggiraldo.misiontic.appturismo.model.repository.RestaurantRepository;

import com.andresggiraldo.misiontic.appturismo.model.repository.UserRepository;
import com.andresggiraldo.misiontic.appturismo.mvp.RegistroMVP;
import com.andresggiraldo.misiontic.appturismo.mvp.RestaurantesMVP;

import java.util.List;

import javax.security.auth.callback.Callback;

public class RestaurantesInteractor implements RestaurantesMVP.Model {

    private RestaurantRepository restaurantRepository;

    public RestaurantesInteractor(Context context) {
        restaurantRepository = RestaurantRepository.getInstance(context);
    }


    @Override
    public void obtenerRestaurantes(RestauranteCallback callback) {
               restaurantRepository.getAll(new RestaurantRepository.RestCallback<List<Restaurant>>() {
                   @Override
                   public void onSuccess(List<Restaurant> data) {
                       callback.onSuccess(data);
                   }

                   @Override
                   public void onFailure() {
                       callback.onFailure("Error");

                   }

            });

    }

}
