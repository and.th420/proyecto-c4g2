package com.andresggiraldo.misiontic.appturismo.presenter;

import com.andresggiraldo.misiontic.appturismo.model.RecuperarContrasenaInteractor;
import com.andresggiraldo.misiontic.appturismo.mvp.RecuperarContrasenaMVP;

public class RecuperarContrasenaPresenter implements RecuperarContrasenaMVP.Presenter {

    private RecuperarContrasenaMVP.View view;
    private RecuperarContrasenaMVP.Model model;

    public RecuperarContrasenaPresenter(RecuperarContrasenaMVP.View view){
        this.view = view;
        this.model = new RecuperarContrasenaInteractor();
    }

    @Override
    public void recuperarWithEmail() {
        boolean error = false;

        //view.showEmailError("");

        RecuperarContrasenaMVP.RecuperarInfo recuperarInfo = view.getRecuperarInfo();
        if (recuperarInfo.getEmail().trim().isEmpty()) {
            view.showEmailError("Correo obligatorio");
            error = true;
        } else if (!isEmailValid(recuperarInfo.getEmail().trim())) {
            view.showEmailError("Correo no válido");
            error = true;
        }

        if (!error) {
            view.openRecuperarContrasenaActivity();
            //view.showGeneralError("Enviamos un correo electónico con un enlace para que recuperes el acceso a la cuenta.");
        }

        else {
                view.showGeneralError("Verifique los datos");
            }
        }

    private boolean isEmailValid(String email) {
        return email.contains("@");  //&& email.endsWith(".com");
    }

}

