package com.andresggiraldo.misiontic.appturismo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.model.Restaurantes;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterRestaurantes extends RecyclerView.Adapter<AdapterRestaurantes.ViewHolderRestaurantes>{

    List<Restaurant> listaRestaurantes;

    public AdapterRestaurantes() {
        this.listaRestaurantes = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolderRestaurantes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderRestaurantes(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_restaurantes,null,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRestaurantes holder, int position) {
        holder.itemNombre.setText(listaRestaurantes.get(position).getNombre());
        //holder.item_grid_imagen

        Glide.with(holder.itemView.getContext())
                .load(getImage(holder.itemView.getContext(),listaRestaurantes.get(position).getFoto()))
                .centerCrop()
                .into(holder.itemGridImagen);
    }

    public int getImage(Context context, String imageName) {
        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        return drawableResourceId;

    }

    @Override
    public int getItemCount() {
        return listaRestaurantes.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Restaurant> listaRestaurantes) {
        this.listaRestaurantes = listaRestaurantes;
        notifyDataSetChanged();
    }

    public class ViewHolderRestaurantes extends RecyclerView.ViewHolder {

        TextView itemNombre;
        ImageView itemGridImagen;

        public ViewHolderRestaurantes(@NonNull View itemView) {
            super(itemView);
            itemNombre=itemView.findViewById(R.id.item_nombre);
            itemGridImagen=(ImageView) itemView.findViewById(R.id.item_grid_imagen);
        }

    }
}


