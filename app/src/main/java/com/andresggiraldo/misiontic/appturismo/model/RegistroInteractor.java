package com.andresggiraldo.misiontic.appturismo.model;

import android.content.Context;

import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;
import com.andresggiraldo.misiontic.appturismo.model.repository.FirebaseAuthRepository;
import com.andresggiraldo.misiontic.appturismo.model.repository.UserRepository;
import com.andresggiraldo.misiontic.appturismo.mvp.RegistroMVP;


public class RegistroInteractor implements RegistroMVP.Model {

    private UserRepository userRepository;
    private FirebaseAuthRepository firebaseAuthRepository;

    public RegistroInteractor(Context context) {
        userRepository = UserRepository.getInstance(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    @Override
    public void registro(User userRegistro, RegistroUserCallback callback) {
        userRepository.insert(userRegistro);
        userRepository.getUserByEmail(userRegistro.getEmail(), new UserRepository.UserCallback<User>() {
            @Override
            public void onSuccess(User data) {
                firebaseAuthRepository.registerNewUser(data, new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure("Error al guardar datos");
                    }
                });

            }

            @Override
            public void onFailure() {
                callback.onFailure("Error al guardar datos");
            }
        });
    }


    @Override
    public void validateUser(String email, ValidateUserCallback callback) {
        userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>() {
            @Override
            public void onSuccess(User user) {
                if (user != null && user.getEmail().equals(email)) {
                    callback.onFailure("Usuario ya existe");
                } else {
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure() {
                callback.onFailure("Error en firebase");
            }
        });

    }
}



