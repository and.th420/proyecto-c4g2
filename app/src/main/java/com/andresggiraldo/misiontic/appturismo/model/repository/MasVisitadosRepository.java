package com.andresggiraldo.misiontic.appturismo.model.repository;

import android.content.Context;

import com.andresggiraldo.misiontic.appturismo.model.MasVisitados;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Visitados;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class MasVisitadosRepository {

    private static MasVisitadosRepository instance;

    public static MasVisitadosRepository getInstance(Context context) {
        if (instance == null) {
            instance = new MasVisitadosRepository(context);
        }
        return instance;
    }

    private DatabaseReference MasVisitadosRef;

    public MasVisitadosRepository(Context context) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        MasVisitadosRef = database.getReference("MasVisitados");


    }



    public void getAll(MasVisitadosRepository.MasVisitadosCallback<List<Visitados>> callback) {
        MasVisitadosRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DataSnapshot dataSnapshot = task.getResult();
                if (dataSnapshot.hasChildren()) {
                    List<Visitados> masvisitados = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Visitados masvisitado = snapshot.getValue(Visitados.class);
                        masvisitados.add(masvisitado);
                    }
                    callback.onSuccess(masvisitados);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }
            } else {
                callback.onFailure();
            }
        });
    }

    public interface MasVisitadosCallback<T> {
        void onSuccess(T data);
        void onFailure();
    }
}