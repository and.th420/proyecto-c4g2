package com.andresggiraldo.misiontic.appturismo.mvp;

import android.app.Activity;

import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;

public interface RegistroMVP {

    interface Model {
        void registro(User user,RegistroUserCallback callback);


        interface RegistroUserCallback{
            void onSuccess();
            void onFailure(String error);
        }

        void validateUser(String email, ValidateUserCallback callback);
        interface ValidateUserCallback {
            void onSuccess();
            void onFailure(String error);
        }


    }

    interface Presenter {
        void registrarse();

    }

    interface View{
        Activity getActivity();
        RegistroInfo getRegistroInfo();
        void showNombresError(String error);
        void showApellidosError(String error);
        void showEdadError(String error);
        void showCuidadError(String error);
        void showEmailError(String error);
        void showContraseñaError(String error);
        void showConfirmarContraseñaError(String error);
        void showGeneralError(String error);

        void clearData();

        void startWaiting();
        void openHomeActivity();



    }

    class RegistroInfo{
        private final String nombres;
        private String apellidos;
        private String edad;
        private String cuidad;
        private String email;
        private String contraseña;
        private String confirmarContraseña;

        public RegistroInfo(String nombres, String apellidos, String edad, String cuidad, String email, String contraseña, String confirmarContraseña) {
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.edad = edad;
            this.cuidad = cuidad;
            this.email = email;
            this.contraseña = contraseña;
            this.confirmarContraseña = confirmarContraseña;
        }

        public String getNombres() {
            return nombres;
        }

        public String getApellidos() {
            return apellidos;
        }

        public String getEdad() {
            return edad;
        }

        public String getCuidad() {
            return cuidad;
        }

        public String getEmail() {
            return email;
        }

        public String getContraseña() {
            return contraseña;
        }

        public String getConfirmarContraseña() {
            return confirmarContraseña;
        }
    }

}
