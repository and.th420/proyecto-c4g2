package com.andresggiraldo.misiontic.appturismo.mvp;

import androidx.appcompat.app.AlertDialog;

public interface RecuperarContrasenaMVP {

    interface Model{

        boolean validateCredentials(String email);
    }

    interface Presenter{
        void recuperarWithEmail();
    }

    interface View{
        RecuperarInfo getRecuperarInfo();

        void showEmailError(String error);
        void showGeneralError(String error);

        void clearData();

        AlertDialog openRecuperarContrasenaActivity();

    }

    class RecuperarInfo {
        private String email;


        public RecuperarInfo(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }
    }
}
