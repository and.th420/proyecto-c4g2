package com.andresggiraldo.misiontic.appturismo.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.mvp.RecuperarContrasenaMVP;
import com.andresggiraldo.misiontic.appturismo.presenter.RecuperarContrasenaPresenter;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

public class RecuperarContrasenaActivity extends AppCompatActivity implements RecuperarContrasenaMVP.View {


    private TextInputEditText recuperar_email;


    private AppCompatButton reg_recuperar;

    private RecuperarContrasenaMVP.Presenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasena);
        ImageView background = findViewById(R.id.background);








        Glide.with(this)
                .load(getImage("bakground"))
                .centerCrop()
                .into(background);


        presenter = new RecuperarContrasenaPresenter(this);

        initUI();
    }


    private void initUI() {
        recuperar_email = findViewById(R.id.recuperar_email);
        reg_recuperar = findViewById(R.id.reg_recuperar);
        reg_recuperar.setOnClickListener(evt -> presenter.recuperarWithEmail());



    }







    public int getImage(String imageName) {

        return this.getResources().getIdentifier(imageName, "drawable", this.getPackageName());

    }

    @Override
    public RecuperarContrasenaMVP.RecuperarInfo getRecuperarInfo() {
        return new RecuperarContrasenaMVP.RecuperarInfo(recuperar_email.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        recuperar_email.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(RecuperarContrasenaActivity.this, error, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void clearData() {
        recuperar_email.setError("");
        recuperar_email.setText("");
    }

        //Mensaje-popup
    @Override
    public AlertDialog openRecuperarContrasenaActivity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RecuperarContrasenaActivity.this);

        //builder.setTitle("Planner Travel")
                builder.setMessage("Enviamos un correo electónico con un enlace para que recuperes el acceso a la cuenta.")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent( RecuperarContrasenaActivity.this, LoginActivity.class);
                                startActivity(intent.createChooser(intent,"Ok"));

                            }

                        });
                    return builder.show();


    }







    }




















