package com.andresggiraldo.misiontic.appturismo.model.repository;

import android.content.Context;
import android.util.Log;

import com.andresggiraldo.misiontic.appturismo.model.database.TurismoDatabase;
import com.andresggiraldo.misiontic.appturismo.model.database.dao.UserDao;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance;

    public static UserRepository getInstance(Context context) {
        if (instance == null) {
            instance = new UserRepository(context);
        }
        return instance;
    }

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    private UserDao userDao;

    // Write a message to the database

    private DatabaseReference userRef;

    private UserRepository(Context context) {
        userDao = TurismoDatabase.getDatabase(context).getUserDao();


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("users");

        loadInitalDatabase();
    }
    private void loadInitalDatabase() {
        if (USE_DATABASE) {
            userDao.insert(
                    new User("Andres", "apellido","25","Bta", "and@email.com","12345678"),
                    new User("Usuario", "de Pruebas", "26","Bogota", "87654321", "87654321")
            );
        } else {
            String username = "cdiaz@email.com".replace('@', '_').replace('.', '_');

            userRef.child(username).child("name").setValue("Andres");
            userRef.child(username).child("email").setValue("and@email.com");
            userRef.child(username).child("password").setValue("12345678");

            username = "test@email.com".replace('@', '_').replace('.', '_');
            userRef.child(username)
                    .setValue(new User("Usuario", "de Pruebas", "26","Bogota", "87654321", "87654321"));
        }
    }
    public void insert(User newUser) {

        userDao.insert(newUser);

        String username = newUser.getEmail().replace('@', '_').replace('.', '_');
        userRef.child(username).setValue(newUser);
    }



    public void getUserByEmail(String email, UserCallback<User> callback) {
        if (USE_DATABASE) {
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {
            // Usar Firebase
            String username = email.replace('@', '_').replace('.', '_');
            userRef.child(username).get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            User value = task.getResult().getValue(User.class);
                            callback.onSuccess(value);
                        } else {
                            callback.onFailure();
                        }
                    });
        }
    }


    public void getAll(UserCallback<List<User>> callback) {
        userRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DataSnapshot dataSnapshot = task.getResult();
                if (dataSnapshot.hasChildren()) {
                    List<User> users = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        snapshot.child("name").getValue(String.class);

                        User user = snapshot.getValue(User.class);
                        Log.d(UserRepository.class.getSimpleName(), user.toString());
                        users.add(user);
                    }
                    callback.onSuccess(users);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }
            } else {
                callback.onFailure();
            }
        });
    }

public interface UserCallback<T> {
    void onSuccess(T data);

    void onFailure();
}

}
