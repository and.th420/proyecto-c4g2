package com.andresggiraldo.misiontic.appturismo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.model.MasVisitados;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Visitados;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterMasVisitados extends RecyclerView.Adapter<AdapterMasVisitados.ViewHolderMasVisitados>{

    List<Visitados> listaMasVisitados;

    public AdapterMasVisitados() {
        this.listaMasVisitados = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolderMasVisitados onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderMasVisitados(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_masvisitados,null,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderMasVisitados holder, int position) {
        holder.itemNombre.setText(listaMasVisitados.get(position).getNombre());
        //holder.item_grid_imagen

        Glide.with(holder.itemView.getContext())
                .load(getImage(holder.itemView.getContext(),listaMasVisitados.get(position).getFoto()))
                .centerCrop()
                .into(holder.itemGridImagen);
    }

    public int getImage(Context context, String imageName) {
        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        return drawableResourceId;

    }

    @Override
    public int getItemCount() {
        return listaMasVisitados.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Visitados> listaMasVisitados) {
        this.listaMasVisitados = listaMasVisitados;
        notifyDataSetChanged();
    }

    public class ViewHolderMasVisitados extends RecyclerView.ViewHolder {

        TextView itemNombre;
        ImageView itemGridImagen;

        public ViewHolderMasVisitados(@NonNull View itemView) {
            super(itemView);
            itemNombre=itemView.findViewById(R.id.item_nombre);
            itemGridImagen=(ImageView) itemView.findViewById(R.id.item_grid_imagen);
        }

    }
}