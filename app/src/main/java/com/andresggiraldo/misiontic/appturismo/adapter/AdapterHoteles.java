package com.andresggiraldo.misiontic.appturismo.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.model.Hoteles;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterHoteles extends RecyclerView.Adapter<AdapterHoteles.ViewHolderHoteles>{

    ArrayList<Hoteles> listaHoteles;

    public AdapterHoteles() {
        this.listaHoteles = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolderHoteles onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderHoteles(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_hoteles,null,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderHoteles holder, int position) {
        holder.itemNom.setText(listaHoteles.get(position).getNombre());
        //holder.item_grid_imagen

        Glide.with(holder.itemView.getContext())
                .load(listaHoteles.get(position).getFoto())
                .centerCrop()
                .into(holder.itemGridIma);
    }

    @Override
    public int getItemCount() {
        return listaHoteles.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<Hoteles> listaHoteles) {
        this.listaHoteles = listaHoteles;
        notifyDataSetChanged();
    }

    public class ViewHolderHoteles extends RecyclerView.ViewHolder {

        TextView itemNom;
        ImageView itemGridIma;

        public ViewHolderHoteles(@NonNull View itemView) {
            super(itemView);
            itemNom=itemView.findViewById(R.id.item_nom);
            itemGridIma=(ImageView) itemView.findViewById(R.id.item_grid_ima);
        }

    }
}
