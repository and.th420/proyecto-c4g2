package com.andresggiraldo.misiontic.appturismo.model;

import android.content.Context;

import com.andresggiraldo.misiontic.appturismo.model.database.entities.Visitados;
import com.andresggiraldo.misiontic.appturismo.model.repository.MasVisitadosRepository;

import com.andresggiraldo.misiontic.appturismo.model.repository.UserRepository;
import com.andresggiraldo.misiontic.appturismo.mvp.RegistroMVP;
import com.andresggiraldo.misiontic.appturismo.mvp.MasVisitadosMVP;

import java.util.List;

import javax.security.auth.callback.Callback;

public class MasVisitadosInteractor implements MasVisitadosMVP {

    private MasVisitadosRepository masVisitadosRepository;
    private Object Visitados;

    public MasVisitadosInteractor(Context context) {
        masVisitadosRepository = MasVisitadosRepository.getInstance(context);
    }


    @Override
    public void obtenerMasVisitados(MasVisitadosMVP.Model.MasVisitadosCallback callback) { 
                masVisitadosRepository.getAll(new MasVisitadosRepository().RestCallback<List<Visitados>>() {
                    @Override
                    public void onSuccess(List<Visitados> data) { callback.onSuccess(data); }
                    
                    @Override
                    public void onFailure() {
                        callback.onFailure("Error");
    
                    }
                
                });

    }

}
