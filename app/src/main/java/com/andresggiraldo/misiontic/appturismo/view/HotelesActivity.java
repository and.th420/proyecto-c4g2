package com.andresggiraldo.misiontic.appturismo.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andresggiraldo.misiontic.appturismo.R;
import com.andresggiraldo.misiontic.appturismo.adapter.AdapterHoteles;
import com.andresggiraldo.misiontic.appturismo.model.Hoteles;

import java.util.ArrayList;

public class HotelesActivity extends AppCompatActivity {

    AdapterHoteles adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoteles);

        RecyclerView recycleHoteles=findViewById(R.id.hot_recycle);
        recycleHoteles.setLayoutManager(new GridLayoutManager(this,2));

        adapter=new AdapterHoteles();
        recycleHoteles.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        llenarHoteles();
    }

    private void llenarHoteles() {
        ArrayList<Hoteles> listaHoteles = new ArrayList<>();
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel1));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel2));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel3));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel4));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel5));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel1));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel2));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel3));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel4));
        listaHoteles.add(new Hoteles("Hotel",R.drawable.hotel2));


        adapter.setData(listaHoteles);
    }
}