package com.andresggiraldo.misiontic.appturismo.mvp;

import android.app.Activity;

import com.andresggiraldo.misiontic.appturismo.model.MasVisitados;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Visitados;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.User;

import java.util.ArrayList;
import java.util.List;

public interface MasVisitadosMVP {

    interface Model {
        void obtenerMasVisitados(MasVisitadosMVP.Model.MasVisitadosCallback callback);

        interface MasVisitadosCallback {
            void onSuccess(List<Visitados> listaMasVisitados);

            void onFailure(String error);
        }

    }

    interface Presenter {
        void obtemasVisitados();

    }

    interface View {
        Activity getActivity();

        // MasVisitadosInfo getMasVisitadosInfo();

        void setlistaMasVisitados(List<Visitados> listaMasVisitados);


    }

    class MasVisitadosInfo {
        private final String nombres;
        private String foto;


        public MasVisitadosInfo(String nombres, String foto) {
            this.nombres = nombres;
            this.foto = foto;

        }

        public String getNombres() {
            return nombres;
        }

        public String getApellidos() {
            return foto;
        }

    }
}
