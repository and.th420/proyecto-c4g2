package com.andresggiraldo.misiontic.appturismo.presenter;

import com.andresggiraldo.misiontic.appturismo.model.RegistroInteractor;
import com.andresggiraldo.misiontic.appturismo.model.RestaurantesInteractor;
import com.andresggiraldo.misiontic.appturismo.model.database.entities.Restaurant;
import com.andresggiraldo.misiontic.appturismo.mvp.RegistroMVP;
import com.andresggiraldo.misiontic.appturismo.mvp.RestaurantesMVP;

import java.util.List;

public class RestaurantePresenter implements RestaurantesMVP.Presenter {

    private RestaurantesMVP.View view;
    private RestaurantesMVP.Model model;

    public RestaurantePresenter(RestaurantesMVP.View view) {
        this.view = view;
        this.model = new RestaurantesInteractor(view.getActivity());
    }


    @Override
    public void obterestaurante() {

        //RestaurantesMVP.RestauranteInfo restauranteInfo = view.getRestaurantesInfo();


        model.obtenerRestaurantes(
                new RestaurantesMVP.Model.RestauranteCallback() {
                    @Override
                    public void onSuccess(List<Restaurant> listaRestaurantes) {
                        view.setListaRestaurante(listaRestaurantes);

                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
    }
}
